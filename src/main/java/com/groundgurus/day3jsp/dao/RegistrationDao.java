package com.groundgurus.day3jsp.dao;

import com.groundgurus.day3jsp.entity.Registration;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

/**
 *
 * @author pgutierrez
 */
@Stateless
public class RegistrationDao {

    @PersistenceContext
    private EntityManager em;
    
    @Transactional
    public void register(Registration registration) {
        em.persist(registration);
    }
}
